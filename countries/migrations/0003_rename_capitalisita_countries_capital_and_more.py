# Generated by Django 4.1.7 on 2023-03-17 18:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('countries', '0002_rename_capital_countries_capitalisita_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='countries',
            old_name='capitalisita',
            new_name='capital',
        ),
        migrations.RenameField(
            model_name='countries',
            old_name='namesito',
            new_name='name',
        ),
    ]
